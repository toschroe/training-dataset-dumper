#ifndef STREAMERS_HH
#define STREAMERS_HH

#include <ostream>

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

std::ostream& operator<<(std::ostream&, const xAOD::Jet&);

#endif
