#ifndef JET_CONST_WRITER_HH
#define JET_CONST_WRITER_HH

// Standard Library things
#include <string>
#include <vector>

namespace H5 {
  class Group;
}
namespace xAOD {
  class TrackCaloCluster_v1;
  typedef TrackCaloCluster_v1 TrackCaloCluster;
  class Jet_v1;
  typedef Jet_v1 Jet;
}

class JetConstWriterConfig;
class ConstOutputWriter;
class ConstConsumers;

struct ConstOutputs {
  const xAOD::TrackCaloCluster* tcc;
  const xAOD::Jet* jet;
};

class JetConstWriter
{
public:
  typedef std::vector<const xAOD::TrackCaloCluster*> TCCs;
  JetConstWriter(
    H5::Group& output_file,
    const JetConstWriterConfig&);
  ~JetConstWriter();
  JetConstWriter(JetConstWriter&) = delete;
  JetConstWriter operator=(JetConstWriter&) = delete;
  void write(const JetConstWriter::TCCs& tccs, const xAOD::Jet& jet);
  void write_dummy();
private:
  template<typename I, typename O = I>
  void add_const_fillers(ConstConsumers&,
                         const std::vector<std::string>&,
                         O def_value);
  
  ConstOutputWriter* m_hdf5_const_writer;
};

#endif
