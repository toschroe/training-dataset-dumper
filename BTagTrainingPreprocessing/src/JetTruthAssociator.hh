#ifndef JET_TRUTH_ASSOCIATOR_HH
#define JET_TRUTH_ASSOCIATOR_HH

#include "TruthSelectorConfig.hh"

#include "xAODJet/JetFwd.h"
#include "xAODTruth/TruthParticleContainerFwd.h"

#include "xAODBase/IParticleContainer.h"
#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"

#include <string>


class JetTruthAssociator
{
public:
  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::IParticleContainer> PartLink;
  typedef std::vector<PartLink> PartLinks;

  JetTruthAssociator(const std::string& link_name,
                    TruthSelectorConfig = TruthSelectorConfig());

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet, const xAOD::TruthParticleContainer* tpc) const;

  bool passed_cuts(const xAOD::TruthParticle& part, const xAOD::Jet& jet) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<PartLinks> m_deco;
  TruthKinematicConfig m_kinematics;
  std::function<bool(const xAOD::TruthParticle&)> m_selector;
};

#endif
