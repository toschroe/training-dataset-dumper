#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

# config mapping
#
CFG_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../../configs
declare -A CONFIGS=(
    [pflow]=${CFG_DIR}/single-b-tag/EMPFlow.json
    [slim]=${CFG_DIR}/single-b-tag/EMPFlowSlim.json
    [truth]=${CFG_DIR}/single-b-tag/EMPFlowTruth.json
    [truthjets]=${CFG_DIR}/single-b-tag/AktTruthJets.json
    [trackjets]=${CFG_DIR}/single-b-tag/TrackJets.json
    [fatjets]=${CFG_DIR}/single-b-tag/FatJets.json
    [hbb]=${CFG_DIR}/hbb/2020_ftag5dev.json
    [ca]=${CFG_DIR}/single-b-tag/EMPFlowSlim.json
    [minimal]=${CFG_DIR}/single-b-tag/minimal.json
    [retag]=${CFG_DIR}/single-b-tag/EMPFlowSlim.json
    [upgrade]=${CFG_DIR}/single-b-tag/upgrade.json
    [trackless]=${CFG_DIR}/single-b-tag/TracklessEMPFlow.json
    [trigger]=${CFG_DIR}/single-b-tag/trigger.json
    [trigger-emtopo]=${CFG_DIR}/single-b-tag/trigger_emtopo.json
    [trigger-topo-fullprec]=${CFG_DIR}/single-b-tag/trigger_emtopo.json
    [trigger-all]=${CFG_DIR}/single-b-tag/trigger_all.json
    [data]=${CFG_DIR}/single-b-tag/EMPFlowData.json
    [multi]=${CFG_DIR}/single-b-tag/multi.json
)

declare -A DATAFILES=(
    [pflow]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [slim]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [truth]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [truthjets]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [trackjets]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [fatjets]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [hbb]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [ca]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [minimal]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [retag]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
    [upgrade]=r12714/AOD.600012.e8185_s3695_s3700_r12714.pool.root
    [trackless]=p5015/DAOD_FTAG1.800030.e7954_s3778_r13258_r13146_p5015.pool.root
    [trigger]=r13615/AOD.601229.e8357_e7400_s3775_r13615_r13615.pool.root
    [trigger-emtopo]=r13615/AOD.601229.e8357_e7400_s3775_r13615_r13615.pool.root
    [trigger-topo-fullprec]=r13615/AOD.601229.e8357_e7400_s3775_r13615_r13615.pool.root
    [trigger-all]=r13615/AOD.601229.e8357_e7400_s3775_r13615_r13615.pool.root
    [data]=r13546/AOD.00360026.r13546_p5065.pool.root
    [multi]=22.2.X/2022-01-12T2101/DAOD_PHYSVAL.small.pool.root
)
declare -A TESTS=(
    [pflow]=dump-single-btag
    [slim]=dump-single-btag
    [truth]=dump-single-btag
    [truthjets]=dump-single-btag
    [trackjets]=dump-single-btag
    [fatjets]=dump-single-btag
    [hbb]=dump-hbb
    [ca]=ca-dump-single-btag
    [minimal]=ca-dump-minimal-btag
    [retag]=ca-dump-retag
    [upgrade]=ca-dump-upgrade
    [trackless]=dump-single-btag
    [trigger]=ca-dump-trigger-pflow
    [trigger-emtopo]=ca-dump-trigger-emtopo
    [trigger-topo-fullprec]="ca-dump-trigger-emtopo -p"
    [trigger-all]=ca-dump-trigger-all
    [data]=dump-single-btag
    [multi]=ca-dump-multi-config
)

################################################
# parse arguments
################################################

ALL_MODES=${!CONFIGS[*]}

print-usage() {
    echo "usage: ${0##*/} [-rh] [-d <dir>] (${ALL_MODES[*]// /|})" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and use it to
produce a training dataset.

Options:
 -d <dir>: specify directory to run in
 -r: build (and run on) a reduced test file
 -h: print help

If no -d argument is given we'll create one in /tmp and work there.

EOF
    exit 1
}

DIRECTORY=""
DATA_URL=https://dguest-ci.web.cern.ch/dguest-ci/ci/ftag/dumper
REDUCE_INPUT=''

while getopts ":d:hr" o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        h) help ;;
        r) REDUCE_INPUT=1 ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1


############################################
# Check that all the modes / paths exist
############################################
#
if [[ ! ${CONFIGS[$MODE]+x} ]]; then usage; fi
CFG=${CONFIGS[$MODE]}

if [[ ! ${DATAFILES[$MODE]+x} ]]; then usage; fi
DOWNLOAD_PATH=${DATAFILES[$MODE]}
FILE=${DOWNLOAD_PATH##*/}

if [[ ! ${TESTS[$MODE]+x} ]]; then usage; fi
RUN=${TESTS[$MODE]}


############################################
# Define the run command
############################################

# basic run command
function run-test-basic {
    local CMD="$RUN $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}

# run on reduced input file
function run-test-reduced {
    local REDUCED=DAOD_SMALL.pool.root
    local REDCMD="ca-make-test-file $FILE -c $CFG -o $REDUCED"
    echo "running ${REDCMD}"
    ${REDCMD}
    cat <<EOF

#################### successfully ran reduction ####################
now processing $REDUCED to HDF5
####################################################################

EOF
    local CMD="$RUN $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}

if [[ ${REDUCE_INPUT} ]] ; then
    if [[ ${MODE} != @(ca|minimal) ]] ; then
        echo "Input reduction is not supported in '${MODE}' mode" >&2
        exit 1
    fi
    RUN_TEST=run-test-reduced
else
    RUN_TEST=run-test-basic
fi


#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

# get files
if [[ ! -f ${FILE} ]] ; then
    echo "getting file ${FILE}" >&2
    curl -s ${DATA_URL}/${DOWNLOAD_PATH} > ${FILE}
fi

# now run the test
${RUN_TEST}

# require some jets were written
echo "========== output summary ==========="
test-output output.h5
